module.exports = {
  ...require("prettier-config-standard"),
  printWidth: 120,
  singleQuote: false,
  semi: true,
  arrowParens: "avoid",
  endOfLine: "auto",
  bracketSpacing: false
};
