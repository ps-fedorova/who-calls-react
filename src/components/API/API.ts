import axios, {AxiosResponse} from "axios";

export function sendRequest(requestURL: string): Promise<AxiosResponse<any, any>> {
  return axios.get<any>(requestURL);
}
