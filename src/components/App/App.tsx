import React, {useContext} from "react";
import "../../vendor/normalize.css";
import "../../vendor/fonts/inter.css";
import "./App.css";
import {ModalContext} from "../../context/ModalContext";
import {useActivation} from "../../hooks/useActivation";
import Header from "../Header/Header";
import Main from "../Main/Main";
import Footer from "../Footer/Footer";
import {PopupModal} from "../Popup/PopupModal";
import PopupContent from "../Popup/PopupContent";

function App(): JSX.Element {
  const {isOpen, openModal, closeModal} = useContext(ModalContext);
  const {
    fetchData,
    popupData,
    error,
    loading,
    enterAnotherTel,
    onChangePopupId,
    id,
    userInput,
    onChangeUserInput,
    disabled,
    popupInputDesc,
    runTimer
  } = useActivation();

  const handleActivate = (): void => {
    openModal();
    fetchData().then(res => res);
  };

  return (
    <div className={isOpen ? "block-content" : ""}>
      <Header onChange={onChangePopupId} id={id} />
      <Main handleActivate={handleActivate} />
      <Footer />

      <PopupModal onClose={closeModal} isOpen={isOpen}>
        <PopupContent
          loading={loading}
          popupData={popupData}
          enterAnotherTel={enterAnotherTel}
          userInput={userInput}
          onChangeUserInput={onChangeUserInput}
          disabled={disabled}
          popupInputDesc={popupInputDesc}
          runTimer={runTimer}
          error={error}
        />
      </PopupModal>
    </div>
  );
}

export default App;
