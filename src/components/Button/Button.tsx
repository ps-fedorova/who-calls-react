import "./Button.css";
import React from "react";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  buttonText: string;
  onClick: () => void;
  buttonClassName?: string;
}

export default function Button(props: ButtonProps): JSX.Element {
  const {buttonText, onClick, type, buttonClassName = "", disabled} = props;

  return (
    <button className={`button ${buttonClassName}`} onClick={onClick} type={type} disabled={disabled}>
      {buttonText}
    </button>
  );
}
