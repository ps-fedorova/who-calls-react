import "./Footer.css";

export default function Footer(): JSX.Element {
  return (
    <footer className="footer common-paddings">
      <nav className="navigation footer__navigation">
        <h2 className="navigation__title">Помощь</h2>
        <div className="navigation-menu">
          <ul className="navigation-menu__list">
            <li className="navigation-menu__item">
              <a className="navigation-menu__link" href="#" rel="nofollow noopener noreferrer">
                Условия Kaspersky WhoCalls
              </a>
            </li>
          </ul>
        </div>
      </nav>

      <p className="footer__copyright">&#169;&nbsp;1993–2022 ПАО&nbsp;«МегаФон»</p>
    </footer>
  );
}
