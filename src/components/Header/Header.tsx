import megafon_big from "../../images/megafon_big.png";
import megafon_small from "../../images/megafon_small.png";
import "./Header.css";
import React from "react";

interface HeaderProps {
  id: string;
  onChange: (evt: React.ChangeEvent<HTMLInputElement>) => void;
}

export default function Header(props: HeaderProps) {
  return (
    <header className="header">
      <img
        srcSet={`${megafon_small} 89w, ${megafon_big} 163w`}
        sizes="(max-width: 768px) 89px, 163px"
        alt="МегаФон"
        src={megafon_small}
      />

      <div style={{fontSize: 8, position: "absolute", right: "10px", top: "10px", textAlign: "right", zIndex: 1}}>
        <p>Инпут временный, чтобы отправлять запросы на бэк</p>
        <p>Введите число от 1 до 7</p>
        <input id={"id"} onChange={props.onChange} value={props.id} />
      </div>
    </header>
  );
}
