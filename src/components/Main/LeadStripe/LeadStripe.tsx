import "./LeadStripe.css";
import handset_small from "../../../images/handset_small.png";
import handset_big from "../../../images/handset_big.png";
import Button from "../../Button/Button";

interface LeadStripeProps {
  handleActivate: () => void;
}

export default function LeadStripe({handleActivate}: LeadStripeProps) {
  return (
    <section className="lead-stripe common-paddings">
      <figure className="lead-stripe__logo">
        <img
          srcSet={`${handset_small} 79w, ${handset_big} 308w`}
          sizes="(max-width: 768px) 79px, 308px"
          alt="МегаФон"
          src={handset_small}
          className="lead-stripe__img"
        />
        <figcaption className="lead-stripe__logo-label">Kaspersky Who&nbsp;Calls</figcaption>
      </figure>

      <div className="lead-stripe__description">
        <h1 className="lead-stripe__title">
          <p className="lead-stripe__title-highlight">Безопасное общение</p>
          <p>без&nbsp;звонков спамеров и&nbsp;мошенников</p>
        </h1>
        <div className="lead-stripe__text-container">
          <p className="lead-stripe__text">
            Удобный и&nbsp;надежный определитель номера, который позволяет вам точно знать, кто звонит,
            и&nbsp;обеспечивает безопасное пространство для общения.
          </p>
          <span className="lead-stripe__badge lead-stripe__badge_type_tablet">
            <span className="lead-stripe__badge-text-row lead-stripe__badge-text-row_highlighted">7 дней</span>
            <span className="lead-stripe__badge-text-row">бесплатно</span>
          </span>
        </div>
        <div className="lead-stripe__actions">
          <Button type={"button"} onClick={handleActivate} buttonText={"Подключить за\u00A07₽\u00A0в\u00A0день"} />
          <span className="lead-stripe__badge lead-stripe__badge_type_mobile">
            <span className="lead-stripe__badge-text-row lead-stripe__badge-text-row_highlighted">7 дней</span>
            <span className="lead-stripe__badge-text-row">бесплатно</span>
          </span>
        </div>
      </div>
      <span className="lead-stripe__border" />
    </section>
  );
}
