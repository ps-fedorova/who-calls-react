import SubscriptionOptions from "./SubscriptionOptions/SubscriptionOptions";
import LeadStripe from "./LeadStripe/LeadStripe";
import "./Main.css";

interface MainProps {
  handleActivate: () => void;
}

export default function Main({handleActivate}: MainProps): JSX.Element {
  return (
    <main className="content">
      <LeadStripe handleActivate={handleActivate} />
      <SubscriptionOptions handleActivate={handleActivate}/>
    </main>
  );
}
