import "./SubscriptionOptions.css";
import show from "../../../images/show.svg";
import book from "../../../images/book.svg";
import safety from "../../../images/safety.svg";
import promo from "../../../images/promo.svg";
import Button from "../../Button/Button";

const subscriptionOptions = [
  {
    id: "show",
    img: show,
    text: "Определение мошенников и спама"
  },
  {
    id: "book",
    img: book,
    text: "Определение названия и категории организации"
  },
  {
    id: "safety",
    img: safety,
    text: "Блокировка входящих номеров по категориям"
  },
  {
    id: "promo",
    img: promo,
    text: "Отсутствие рекламы"
  }
];

interface SubscriptionOptionsProps {
  handleActivate: () => void;
}

export default function SubscriptionOptions({handleActivate}: SubscriptionOptionsProps): JSX.Element {
  return (
    <section className="subscription-options common-paddings">
      <h2 className="subscription-options__title">
        Не&nbsp;дайте звонкам с&nbsp;неизвестных номеров застать вас в&nbsp;расплох
      </h2>

      <ul className="subscription-options__list">
        {subscriptionOptions.map(option => {
          return (
            <li className="subscription-options__item" key={option.id}>
              <span className="subscription-options__item-text">{option.text}</span>
              <img className={`subscription-options__item-img ${option.id}`} src={option.img} alt={option.id} />
            </li>
          );
        })}
      </ul>
      <Button
        type={"button"}
        onClick={handleActivate}
        buttonText={"Подключить за\u00A07₽\u00A0в\u00A0день"}
        buttonClassName={"subscription-options__button"}
      />
    </section>
  );
}
