import React from "react";
import InputMask from "react-input-mask";
import Button from "../Button/Button";
import "./Popup.css";
import Throbber from "../Throbber/Throbber";
import {PopupData} from "../../data/dataArrayForPopups";
import PopupFlexContainer from "./PopupFlexContainer";
const sec = 119;

interface PopupContentProps {
  loading: boolean;
  popupData: PopupData | null;
  enterAnotherTel: () => void;
  userInput: string;
  onChangeUserInput: (evt: React.ChangeEvent<HTMLInputElement>) => void;
  disabled: boolean;
  popupInputDesc: string;
  runTimer: (sec: number) => void;
  error: string;
}

export default function PopupContent(props: PopupContentProps) {
  const {loading, popupData, enterAnotherTel, disabled, popupInputDesc} = props;

  return loading ? (
    <PopupFlexContainer>
      <p className={"popup__wait-throbber"}>Подождите, пожалуйста</p>
      <Throbber throbberClassName={"popup__wait-throbber"} />
    </PopupFlexContainer>
  ) : (
    renderContent()
  );

  function getSubtitle(): JSX.Element | null {
    if (!popupData) return null;

    const {subtitle, popupSubtitleOtherNumber} = popupData;
    if (!subtitle) return null;

    return (
      <p className="popup__subtitle">
        <span className="popup__subtitle_text">{subtitle}</span>
        {!!popupSubtitleOtherNumber && (
          <span className="popup__subtitle_number_container">
            <span className="popup__subtitle_number">{"+7 XXX XXX XX XX"}</span>
            <button className="popup__subtitle-other-number" onClick={enterAnotherTel}>
              {popupSubtitleOtherNumber}
            </button>
          </span>
        )}
      </p>
    );
  }

  function getImage(): JSX.Element | null {
    if (!popupData) return null;
    const {img, alt, description, withInput} = popupData;

    return (
      <>
        <div className={"popup__image-container"}>
          <img src={img} alt={alt} className="popup__image" />
        </div>
        {!withInput && description && <p className="popup__image-desc">{description}</p>}
      </>
    );
  }

  function getInput(): JSX.Element | null {
    if (!popupData) return null;
    const {placeholder, inputType, withInput, mask} = popupData;
    if (!withInput) return null;

    return (
      <label className="popup__input-container">
        <InputMask
          className="popup__input"
          value={props.userInput}
          onChange={props.onChangeUserInput}
          placeholder={placeholder}
          autoComplete="off"
          type={inputType}
          mask={mask || ""}
          maskChar={""}
        />
        <span className="popup__input-validate">{popupData?.id === 7 && popupInputDesc}</span>
      </label>
    );
  }

  function onClick() {
    popupData?.id === 7 && props.runTimer(sec);
  }

  function renderContent(): JSX.Element {
    if (props.error) return <PopupFlexContainer>{props.error}</PopupFlexContainer>;
    if (!popupData) return <PopupFlexContainer>Что-то пошло не так...</PopupFlexContainer>;

    const {title, buttonText} = popupData;
    return (
      <>
        <p className="popup__title">{title}</p>
        {getSubtitle()}
        {getImage()}
        {getInput()}
        <Button
          type="submit"
          buttonClassName="popup__button"
          onClick={onClick}
          buttonText={buttonText}
          disabled={disabled || (!!popupInputDesc && popupData.id === 7)}
        />
      </>
    );
  }
}
