import Throbber from "../Throbber/Throbber";
import React from "react";

interface PopupFlexContainerProps {
  children: React.ReactNode;
}

export default function PopupFlexContainer(props: PopupFlexContainerProps) {
  const {children} = props;
  return <div className={"popup__wait"}>{children}</div>;
}
