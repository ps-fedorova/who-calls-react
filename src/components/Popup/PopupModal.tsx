import React from "react";
import close from "../../images/close.svg";
import "./Popup.css";

interface ModalProps {
  children: React.ReactNode;
  onClose: () => void;
  isOpen: boolean;
}

export function PopupModal({children, onClose, isOpen}: ModalProps) {
  return (
    <div className={`popup${isOpen ? " popup_open" : ""}`}>
      <div className="popup__container">
        <img className="popup__close" src={close} alt="Закрыть" onClick={onClose} />
        {children}
      </div>
      <div className="popup__overlay" onClick={onClose} />
    </div>
  );
}
