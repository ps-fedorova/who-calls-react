import "./Throbber.css";

interface IThrobber {
  throbberClassName?: string;
}

export default function Throbber({throbberClassName}: IThrobber): JSX.Element {
  return (
    <div className={`throbber ${throbberClassName}`}>
      <span className="throbber__bouncer" />
      <span className="throbber__bouncer" />
      <span className="throbber__bouncer" />
    </div>
  );
}
