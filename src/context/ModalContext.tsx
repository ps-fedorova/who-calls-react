import React, {createContext, useCallback, useEffect, useState} from "react";

interface IModalContext {
  isOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
}

export const ModalContext = createContext<IModalContext>({
  isOpen: false,
  openModal: () => {},
  closeModal: () => {}
});

export const ModalState = ({children}: {children: React.ReactNode}) => {
  const [isOpen, setOpen] = useState(false);
  const openModal = () => setOpen(true);
  const closeModal = () => setOpen(false);

  const handleEsc = useCallback((evt: KeyboardEvent): void => {
    if (evt.key === "Escape") {
      closeModal();
    }
  }, []);

  useEffect(() => {
    window.addEventListener("keydown", handleEsc);
    return () => {
      window.removeEventListener("keydown", handleEsc);
    };
  }, [handleEsc]);

  return <ModalContext.Provider value={{isOpen, openModal, closeModal}}>{children}</ModalContext.Provider>;
};
