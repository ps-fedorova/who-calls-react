import smile from "../images/smiling_face.png";
import sadFace from "../images/sad_face.png";
import angryFace from "../images/angry_face.png";
import manWithBook from "../images/man_with_book.svg";

export interface PopupData {
  id: number;
  title: string;
  subtitle?: string;
  popupSubtitleOtherNumber?: string;
  img: string;
  alt: string;
  description?: string;
  buttonText: string;
  placeholder?: string;
  mask?: string;
  inputType?: "tel" | "numeric";
  withInput?: boolean;
}

export const dataArrayForPopups: PopupData[] = [
  {
    id: 1,
    title: "Успешно",
    img: smile,
    alt: "smile",
    description: "Мы отправим вам подробности в SMS",
    buttonText: "Перейти в сервис"
  },
  {
    id: 2,
    title: "Запрет на\u00A0подключение\u00A0услуг",
    img: sadFace,
    alt: "sad face",
    description: "Позвоните по бесплатному номеру 0500, чтобы снять ограничения",
    buttonText: "Позвонить 0500"
  },
  {
    id: 3,
    title: "Недостаточно средств",
    img: angryFace,
    alt: "angry face",
    description: "Пополнить баланс можно быстро и без комиссии можно на нашем сайте или в терминале салона",
    buttonText: "Пополнить баланс"
  },
  {
    id: 4,
    title: "Что-то пошло не так",
    img: sadFace,
    alt: "sad face",
    description: "Попробуйте перезагрузить страницу и повторить попытку",
    buttonText: "Попробовать снова"
  },
  {
    id: 5,
    title: "Услуга уже была ранее подключена",
    img: sadFace,
    alt: "sad face",
    description: "Промо-код действует только для новых пользователей сервиса.",
    buttonText: "Подключить за\u00A07₽\u00A0в\u00A0день"
  },
  {
    id: 6,
    title: "Войти в сервис",
    subtitle: "Введите номер телефона и мы отправим на него SMS c кодом",
    img: manWithBook,
    alt: "man with a book",
    buttonText: "Войти",
    placeholder: "+7 XXX XXX XX XX",
    mask: "+7 999 999 99 99",
    inputType: "tel",
    withInput: true
  },
  {
    id: 7,
    title: "Войти в сервис",
    subtitle: "Мы\u00A0отправили SMS c\u00A0кодом на\u00A0номер ",
    popupSubtitleOtherNumber: "Другой номер",
    img: manWithBook,
    alt: "man with a book",
    buttonText: "Войти",
    placeholder: "XXXX",
    mask: "9999",
    inputType: "tel",
    withInput: true
  }
];
