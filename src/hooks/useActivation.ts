import React, {useState} from "react";
import {sendRequest} from "../components/API/API";
import {AxiosError} from "axios";
import {dataArrayForPopups, PopupData} from "../data/dataArrayForPopups";
import {isValid} from "../utils/utils";

interface UseActivation {
  fetchData: () => Promise<void>;
  popupData: PopupData | null;
  disabled: boolean;
  error: string;
  loading: boolean;
  enterAnotherTel: () => void;
  onChangePopupId: (evt: React.ChangeEvent<HTMLInputElement>) => void;
  id: string;
  userInput: string;
  onChangeUserInput: (evt: React.ChangeEvent<HTMLInputElement>) => void;
  popupInputDesc: string;
  runTimer: (sec: number) => void;
}

export function useActivation(): UseActivation {
  const [popupData, setPopupData] = useState<PopupData | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [disabled, setDisabled] = useState<boolean>(false);
  const [userInput, setUserInput] = useState<string>("");
  const [popupInputDesc, setPopupInputDesc] = useState<string>("");
  const [id, setId] = useState<string>("7");
  const requestURL = (id: string): string => `https://jsonplaceholder.typicode.com/users/${id}`;

  function onChangePopupId(evt: React.ChangeEvent<HTMLInputElement>): void {
    setId(evt.target.value);
    requestURL(id);
  }

  function onChangeUserInput(evt: React.ChangeEvent<HTMLInputElement>): void {
    const value = evt.target.value?.replace(/\D/, "");
    setUserInput(value);
    const validationResult = isValid(value.replaceAll(" ", ""), id);
    setDisabled(validationResult);
  }

  async function fetchData(): Promise<void> {
    try {
      setError("");
      setLoading(true);
      const response = await sendRequest(requestURL(id));
      handleSuccess(response);
    } catch (e: unknown) {
      const error = e as AxiosError;
      setLoading(false);
      setError(error.message);
    } finally {
      setLoading(false);
    }
  }

  function setDataIndex(id: number): number | undefined {
    let dataIndex: number | undefined;
    dataArrayForPopups.forEach((el, idx) => {
      if (el.id === id) {
        dataIndex = idx;
      }
    });
    return dataIndex;
  }

  function handleSuccess(res: any): void {
    const resId: number = res?.data.id;
    if (id === null || id === undefined) return;

    if (resId === 6 || (resId === 7 && isValid(userInput))) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
    const dataIndex = setDataIndex(resId);
    if (dataIndex === undefined) return;
    setPopupData(dataArrayForPopups[dataIndex]);
    resId !== 7 && setUserInput("");
  }

  function enterAnotherTel(): void {
    setPopupData(dataArrayForPopups["5"]);
    setId("6");
    setUserInput("");
    setDisabled(true);
  }

  function runTimer(sec: number): void {
    setDisabled(true);
    const timer = setInterval(() => {
      const seconds = sec % 60;
      const minutes = (sec / 60) % 60;
      if (sec <= 0) {
        clearInterval(timer);
        setPopupInputDesc("");
        setDisabled(false);
      } else {
        setPopupInputDesc(`Отправить код повторно (${Math.trunc(minutes)}:${seconds})`);
      }
      --sec;
    }, 1000);
  }

  return {
    fetchData,
    popupData,
    disabled,
    error,
    loading,
    enterAnotherTel,
    onChangePopupId,
    id,
    userInput,
    onChangeUserInput,
    popupInputDesc,
    runTimer
  };
}
