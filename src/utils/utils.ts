// валидация

function validatePhone(phone: string): boolean {
  const regex = /[0-9]{11}/;
  return regex.test(phone);
}

function validateCodeFromSms(value: string): boolean {
  const regex = /[0-9]{4}/;
  return regex.test(value);
}

export function isValid(value: string, id?: string): boolean {
  return (id === "6" && !validatePhone(value)) || (id === "7" && !validateCodeFromSms(value));
}
